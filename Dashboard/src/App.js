import React, { useState, useEffect } from "react";
import "./App.css";
import "./index.css";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import Login from "./pages/login";
import ActivateBlock from "./pages/activateBlock";
import SideNavBar from "./pages/sidenavbar";
import Dashboard from "./pages/dashboard";

function App() {
  const [currentUser, setCurrentUser] = useState(null);
  const [isVisible, setIsVisible] = useState(false);

  useEffect(() => {
    if (currentUser !== null) {
      setIsVisible(true);
    } else {
      setIsVisible(false);
    }
  }, [currentUser]);

  return (
    <Router>
      <div>
        <SideNavBar />
        <Routes>
          {/* Define a route for the login page */}
          <Route path="/" element={<ActivateBlock />} />
          <Route path="/dashboard" element={<Dashboard />} />
          <Route path="/ActivateBlock" element={<ActivateBlock />} />
          <Route
            path="/login"
            element={<Login setCurrentUser={setCurrentUser} />}
          />
        </Routes>
      </div>
    </Router>
  );
}

export default App;
