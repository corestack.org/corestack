import React, { useState } from "react";
import { Button, TextField, Typography } from "@mui/material";
import { useNavigate } from "react-router-dom";
import { authenticate } from "../services&apis/authenticate";

const Login = ({ setCurrentUser }) => {
  const navigate = useNavigate();

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [emailErr, setEmailErr] = useState("");
  const [passwordErr, setPasswordErr] = useState("");
  const [loginErr, setLoginErr] = useState("");

  const formInputChange = (formField, value) => {
    if (formField === "email") {
      setEmail(value);
    }
    if (formField === "password") {
      setPassword(value);
    }
  };

  const validation = () => {
    return new Promise((resolve, reject) => {
      if (email === "" && password === "") {
        setEmailErr("Email is Required");
        setPasswordErr("Password is required");
        resolve({
          email: "Email is Required",
          password: "Password is required",
        });
      } else if (email === "") {
        setEmailErr("Email is Required");
        resolve({ email: "Email is Required", password: "" });
      } else if (password === "") {
        setPasswordErr("Password is required");
        resolve({ email: "", password: "Password is required" });
      } else if (password.length < 6) {
        setPasswordErr("Password must be 6 characters long");
        resolve({ email: "", password: "Password must be 6 characters long" });
      } else {
        resolve({ email: "", password: "" });
      }
    });
  };

  const handleClick = () => {
    setEmailErr("");
    setPasswordErr("");
    validation().then((res) => {
      if (res.email === "" && res.password === "") {
        authenticate(email, password)
          .then((data) => {
            setLoginErr("");
            console.log("Authentication successful", data);
            localStorage.setItem("token", data.accessToken.jwtToken);
            setCurrentUser(data);
            navigate("/ActivateBlock");
          })
          .catch((err) => {
            console.log("Authentication error", err);
            setLoginErr(err.message);
          });
      }
    });
  };

  return (
    <div className="login">
      <head>
        {/* Head content */}
        <title>Glassmorphism Login Form Tutorial in HTML CSS</title>
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link
          rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css"
        />
        <link
          href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;500;600&display=swap"
          rel="stylesheet"
        />
        {/* Stylesheet */}
        <style media="screen">
          {`
          *,
          *:before,
          *:after {
            padding: 0;
            margin: 0;
            box-sizing: border-box;
          }
          body {
            background-color: #080710;
            font-family: 'Poppins', sans-serif;
          }
          .background {
            width: 430px;
            height: 520px;
            position: absolute;
            transform: translate(-50%, -50%);
            left: 50%;
            top: 50%;
          }
          .background .shape {
            height: 200px;
            width: 200px;
            position: absolute;
            border-radius: 50%;
          }
          .shape:first-child {
            background: linear-gradient(#1845ad, #23a2f6);
            left: -80px;
            top: -80px;
          }
          .shape:last-child {
            background: linear-gradient(to right, #ff512f, #f09819);
            right: -30px;
            bottom: -80px;
          }
          form {
            height: 520px;
            width: 400px;
            background-color: rgba(255, 255, 255, 0.13);
            position: absolute;
            transform: translate(-50%, -50%);
            top: 50%;
            left: 50%;
            border-radius: 10px;
            backdrop-filter: blur(10px);
            border: 2px solid rgba(255, 255, 255, 0.1);
            box-shadow: 0 0 40px rgba(8, 7, 16, 0.6);
            padding: 50px 35px;
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
          }
          form * {
            color: #ffffff;
            letter-spacing: 0.5px;
            outline: none;
            border: none;
          }
          form h3 {
            font-size: 32px;
            font-weight: 500;
            line-height: 42px;
            text-align: center;
            margin-bottom: 20px;
          }
          label {
            display: block;
            margin-top: 30px;
            font-size: 16px;
            font-weight: 500;
          }
          input {
            display: block;
            height: 50px;
            width: 100%;
            background-color: rgba(255, 255, 255, 0.07);
            border-radius: 3px;
            padding: 0 10px;
            margin-top: 8px;
            font-size: 14px;
            font-weight: 300;
          }
          ::placeholder {
            color: #e5e5e5;
          }
          button {
            margin-top: 20px; /* Adjusted margin-top */
            width: 100%;
            background-color: #ffffff;
            color: #080710;
            padding: 15px 0;
            font-size: 18px;
            font-weight: 600;
            border-radius: 5px;
            cursor: pointer;
          }
          `}
        </style>
      </head>
      <body>
        <div className="background">
          <div className="shape"></div>
          <div className="shape"></div>
        </div>
        <form>
          <h3>Login Here</h3>
          <label htmlFor="username">Username</label>
          <input
            type="text"
            placeholder="Email"
            id="username"
            value={email}
            onChange={(e) => formInputChange("email", e.target.value)}
          />
          {emailErr && <Typography color="error">{emailErr}</Typography>}
          <label htmlFor="password">Password</label>
          <input
            type="password"
            placeholder="Password"
            id="password"
            value={password}
            onChange={(e) => formInputChange("password", e.target.value)}
          />
          {passwordErr && <Typography color="error">{passwordErr}</Typography>}
          <div>
            <Button variant="contained" onClick={handleClick}>
              Log In
            </Button>
          </div>
          {loginErr && <Typography color="error">{loginErr}</Typography>}
        </form>
      </body>
    </div>
  );
};

export default Login;
