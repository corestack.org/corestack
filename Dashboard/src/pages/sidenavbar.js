import React, { useState } from "react";
import { LogOut, Eye } from "lucide-react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faTachometerAlt,
  faPlug,
  faCogs,
  faLayerGroup,
} from "@fortawesome/free-solid-svg-icons";
import layersData from "../jsons/layers.json";
import LocationFormComponent from "./locationForm";
import PreviewLayerComponent from "./previewLayer";
import logo from "../assets/core-stack logo.png";
import { useNavigate } from "react-router-dom";
import ActivateBlock from "./activateBlock";
import PlanCreation from "./planCreation";
const Sidebar = () => {
  const navigate = useNavigate();
  const [isLayerOpen, setIsLayerOpen] = useState(false);
  const [selectedLayer, setSelectedLayer] = useState(null);
  const [showLocationForm, setShowLocationForm] = useState(false);
  const [showPreviewLayer, setShowPreviewLayer] = useState(false);
  const [activeItem, setActiveItem] = useState(null);
  const [currentPage, setCurrentPage] = useState("Dashboard");

  const layers = Object.keys(layersData.layers_json).map((key) => {
    const label = key
      .replace(/_/g, " ")
      .replace(/\b\w/g, (char) => char.toUpperCase());
    return {
      label,
      apiUrl: layersData.layers_json[key].api_url,
      showDates: layersData.layers_json[key].show_Dates,
    };
  });

  const menuItems = [
    {
      icon: <FontAwesomeIcon icon={faTachometerAlt} size="lg" />,
      label: "Dashboard",
      href: "/dashboard",
      action: () => {
        setActiveItem("Dashboard");
        setShowPreviewLayer(false);
        setIsLayerOpen(false);
        console.log("dashbord");
        navigate("/dashboard");
      },
    },
    {
      icon: <FontAwesomeIcon icon={faPlug} size="lg" />,
      label: "Activate Block",
      href: "/activate-block",
      action: () => {
        setActiveItem("Activate Block");
        setShowPreviewLayer(false); // Hide Preview Layer
        setShowLocationForm(false); // Hide Location Form
        setIsLayerOpen(false); // Close layer submenu
        navigate("/activate-block");
      },
    },
    {
      icon: <FontAwesomeIcon icon={faCogs} size="lg" />,
      label: "Plan Creation",
      href: "/plan-creation",
      action: () => {
        setActiveItem("Plan Creation");
        setShowPreviewLayer(false); // Hide Preview Layer
        setShowLocationForm(false); // Hide Location Form
        setIsLayerOpen(false); // Close layer submenu
        navigate("/plan-creation");
      },
    },
    {
      icon: <FontAwesomeIcon icon={faLayerGroup} size="lg" />,
      label: "Generate Layers",
      isSubmenu: true,
      layers: layers,
      action: () => {
        setIsLayerOpen(!isLayerOpen);
      },
    },
    {
      icon: <Eye size={20} />,
      label: "Preview Layers",
      isSubmenu: false,
      action: () => {
        setActiveItem("Preview Layers");
        setShowPreviewLayer(true);
        setShowLocationForm(false);
        setIsLayerOpen(false);
        navigate("/preview-layers");
      },
    },
  ];

  const handleLayerClick = (layerLabel) => {
    const layerDetails = layers.find((layer) => layer.label === layerLabel);
    setSelectedLayer(layerDetails);
    setShowLocationForm(true);
    setShowPreviewLayer(false);
    setActiveItem(layerLabel);
    navigate(`/generate-layers/${layerLabel.toLowerCase().replace(/ /g, "-")}`);
  };

  return (
    <div className="relative min-h-screen flex">
      {/* Sidebar */}
      <div className="fixed top-0 left-0 h-screen w-64 bg-gray-800 text-white">
        <div className="flex items-center justify-center h-16 bg-gray-800">
          <img
            src={logo}
            alt="Logo"
            className="h-24 w-24 mt-10 rounded-full object-cover"
          />
        </div>

        <nav className="flex-1 px-4 py-6">
          <ul className="space-y-2 mt-10">
            {menuItems.map((item, index) => (
              <li key={index}>
                {!item.isSubmenu ? (
                  <button
                    onClick={item.action}
                    className={`flex items-center w-full px-2 py-2 rounded-lg transition-colors ${
                      activeItem === item.label
                        ? "bg-gray-700"
                        : "hover:bg-gray-700"
                    }`}
                  >
                    <div className="w-8">{item.icon}</div>
                    <span className="ml-3">{item.label}</span>
                  </button>
                ) : (
                  <div>
                    <button
                      onClick={item.action}
                      className={`flex items-center w-full px-2 py-2 rounded-lg transition-colors ${
                        activeItem === item.label
                          ? "bg-gray-700"
                          : "hover:bg-gray-700"
                      }`}
                    >
                      <div className="w-8">{item.icon}</div>
                      <span className="ml-3">{item.label}</span>
                      <span className="ml-auto">{isLayerOpen ? "▲" : "▼"}</span>
                    </button>
                    {isLayerOpen && (
                      <ul className="pl-6 mt-2 space-y-1">
                        {item.layers.map((layer, idx) => (
                          <li key={idx}>
                            <button
                              onClick={() => handleLayerClick(layer.label)}
                              className={`block px-2 py-1 w-full rounded-lg text-left text-sm ${
                                activeItem === layer.label
                                  ? "bg-gray-600"
                                  : "hover:bg-gray-600"
                              }`}
                            >
                              {layer.label}
                            </button>
                          </li>
                        ))}
                      </ul>
                    )}
                  </div>
                )}
              </li>
            ))}
          </ul>
        </nav>
        <div className="p-4 border-t border-gray-700">
          <button className="flex items-center w-full px-2 py-2 rounded-lg hover:bg-gray-700 transition-colors">
            <div className="w-8">
              <LogOut size={20} />
            </div>
            <span className="ml-3">Logout</span>
          </button>
        </div>
      </div>

      {/* Main Content */}
      <div className="flex-1 ml-64">
        {/* Top Navbar */}
        <nav className="fixed top-0 left-64 right-0 bg-gray-800 text-white h-16 z-20">
          <div className="flex items-center h-full px-4 relative">
            <h1 className="text-xl font-bold mx-auto">NRM Dashboard</h1>
            <div className="absolute right-4 w-8 h-8 rounded-full bg-gray-600 flex items-center justify-center">
              <span className="text-sm">U</span>
            </div>
          </div>
        </nav>
        <div className="h-[calc(100vh-4rem)] flex items-center justify-center">
          <div className="w-full max-w-xl px-6">
            {/* Conditionally Render Based on State */}
            {activeItem === "Preview Layers" ? (
              <PreviewLayerComponent />
            ) : activeItem === "Activate Block" ? (
              <div className="text-center -mt-16">
                <h1 className="text-2xl font-bold">Activate Block</h1>
                <ActivateBlock />{" "}
              </div>
            ) : activeItem === "Plan Creation" ? (
              <div className="text-center -mt-16">
                <h1 className="text-2xl font-bold">Plan Creation </h1>
                <PlanCreation />
              </div>
            ) : showLocationForm && selectedLayer ? (
              <div className="-mt-16">
                <LocationFormComponent
                  apiUrl={selectedLayer.apiUrl}
                  showDates={selectedLayer.showDates}
                  layerName={selectedLayer.label}
                />
              </div>
            ) : (
              <div className="text-center -mt-16">
                <h1 className="text-2xl font-bold">Please select a layer</h1>
              </div>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Sidebar;
