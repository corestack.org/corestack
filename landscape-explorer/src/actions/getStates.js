import locations from "../locations.json";

export default async function getStates() {
    
    try{
        const response = await fetch(`https://geoserver.gramvaani.org/api/v1/proposed_blocks/`, {
            method: "GET",
            headers: {
                "ngrok-skip-browser-warning": "1",
                "Content-Type": "application/json",
            },
        })

        return await response.json()
    }catch(err){
        console.log(err);
        return locations;
    }
}