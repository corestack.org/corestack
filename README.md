# corestack

## About us

CORE Stack: Commoning for Resilience and Equality is a social-tech enterprise dedicated to empowering underserved communities in low-resource and remote areas. Through our innovative participatory tech platforms, we foster community awareness of various pathways for resilience and development, promoting scientific decision-making and improved solidarity. We focus on strengthening community institutions to ensure equity and alignment along these pathways. Our commitment includes responsible AI practices, ethical considerations, and the use of open-source tools for transparent and accountable development. By enhancing understanding within communities and enabling them to manage natural resources, we contribute to a more sustainable, climate-resilient future with a focus on fairness, inclusivity, and environmental sustainability.

## Partners

Our esteemed partners on this journey include renowned institutions like IIT Delhi, IIT Palakkad, Gram Vaani, and Magasool. Strengthening our mission and shared goals  with consistent support and collaborative affiliations with GIZ, RainMatter, FES, Common Grounds, and Tarides.


If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.


## core-stack architecture

![alt text](https://core-stack.org/corestack.jpeg)


## Installation of Backend 

    Step 1 - Clone the corestack repo

    Step 2 - Go to  commons-connect\commons-connect-backend

    Step 3 -  Create a venv by using this command
          
             python -m pip install venv

              python -m venv venv
    
    Step 4 -  Install all the python packages

            pip install -r requirement.txt

    Step 5 - Run django server and test 

           python manage.py runserver


## Installation of Web Component

    Step 1 - Go to  Cmmons-connect\commons-connect-web

    Step 2 - Install all required node library by 

            npm -i

    Step 3 - Run development Server

           npm start dev

    Step 4- If you want to build it for deployment on S3 bucket use this 

            npm build dev

    