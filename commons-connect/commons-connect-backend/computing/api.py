from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from utilities.geoserver_utils import Geoserver
from .utils import generate_block_shape_file_data, kml_to_geojson
from django.core.files.storage import FileSystemStorage
from utilities.constants import KML_PATH


@api_view(["POST"])
def generate_block_layer(request):
    print("Inside generate_block_layer API.")
    try:
        req_body = request.POST.dict()
        state = req_body.get("state").lower()
        district = req_body.get("district").lower()
        block = req_body.get("block").lower()

        response = generate_block_shape_file_data(state, district, block)
        return Response({"Success": response}, status=status.HTTP_201_CREATED)
    except Exception as e:
        print("Exception in generate_block_layer api :: ", e)
        return Response({"Exception": e}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


@api_view(["POST"])
def create_workspace(request):
    print("Inside create_workspace API.")
    try:
        req_body = request.POST.dict()
        workspace = req_body.get("workspace")
        geo = Geoserver()
        response = geo.create_workspace(workspace)
        print(response)
        return Response({"Success": response}, status=status.HTTP_201_CREATED)
    except Exception as e:
        print("Exception in create_workspace api :: ", e)
        return Response({"Exception": e}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


@api_view(["POST"])
def delete_layer(request):
    print("Inside delete_layer API.")
    try:
        req_body = request.POST.dict()
        workspace = req_body.get("workspace")
        layer_name = req_body.get("layer_name")
        geo = Geoserver()
        response = geo.delete_layer(layer_name, workspace)
        print(response)
        return Response({"Success": response}, status=status.HTTP_200_OK)
    except Exception as e:
        print("Exception in delete_layer api :: ", e)
        return Response({"Exception": e}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


@api_view(["POST"])
def upload_kml(request):
    print("Inside upload_kml API.")
    try:
        req_body = request.POST.dict()
        state = req_body.get("state").lower()
        district = req_body.get("district").lower()
        block = req_body.get("block").lower()
        kml_file = request.FILES["file"]

        fs = FileSystemStorage(KML_PATH)
        filename = fs.save(kml_file.name, kml_file)

        kml_to_geojson(state, district, block, KML_PATH + filename)

        return Response(
            {"Success": "Successfully uploaded"}, status=status.HTTP_201_CREATED
        )
    except Exception as e:
        print("Exception in upload_kml api :: ", e)
        return Response({"Exception": e}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
