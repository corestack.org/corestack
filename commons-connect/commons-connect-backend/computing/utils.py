import os
import pandas as pd
import geopandas as gpd, fiona
from shapely import wkt
from shapely.geometry import mapping
from geojson import Feature, FeatureCollection
from utilities.geoserver_utils import Geoserver
import shutil
from utilities.constants import SHAPE_FILES_INPUT_DIR, SHAPE_FILES_OUTPUT_DIR


def generate_shape_files(path):
    gdf = gpd.read_file(path + ".json")
    os.remove(path + ".json")

    gdf.to_file(
        path,
        driver="ESRI Shapefile",
    )
    return path


def convert_to_zip(dir_name):
    return shutil.make_archive(dir_name, "zip", dir_name + "/")


def push_shape_to_geoserver(shape_path):
    geo = Geoserver()
    path = convert_to_zip(shape_path)
    print(path)
    return geo.create_shp_datastore(path=path, workspace="test_workspace")


def generate_block_shape_file_data(state_name, district_name, block_name):
    # Provide the path to the panchayat shapefiles file for the particular state here
    state_shape_file_metadata_df = pd.read_csv(
        os.path.join(SHAPE_FILES_INPUT_DIR, state_name + ".csv")
    )

    # NOTE Filtering the state level shape file to get the geometries for a particular district
    district_shape_file_df = state_shape_file_metadata_df[
        state_shape_file_metadata_df["District name"].str.lower() == district_name
    ]

    district_shape_file_df["Panchayat_Geometry"] = district_shape_file_df[
        "Panchayat_Geometry"
    ].apply(wkt.loads)

    district_shape_file_df = district_shape_file_df[
        (district_shape_file_df["Subdistrict name"].str.lower() == block_name)
    ]

    features = []
    for index, row in district_shape_file_df.iterrows():
        features.append(
            Feature(
                geometry=mapping(row["Panchayat_Geometry"]),
                properties={
                    "Panchayat_ID": row["Panchayat_ID"],
                    "Panchayat_Name": row["Gram_panchayat_name"],
                    "Subdistrict census code": row["Subdistrict census code"],
                    "Subdistrict name": row["Subdistrict name"],
                    "District census code": row["District census code"],
                    "District name": row["District name"],
                    "State census code": row["State census code"],
                    "State name": row["State name"],
                    "ADI 2011": row["adi_2011"],
                    "ADI 2019": row["adi_2019"],
                    "Number_of_Households": row["no_hh"],
                    "Total_Population": row["tot_p"],
                    "Total_Male_Population": row["tot_m"],
                    "Total_Female_Population": row["tot_f"],
                    "Total_SC_Population": row["p_sc"],
                    "Male_SC_Population": row["m_sc"],
                    "Female_SC_Population": row["f_sc"],
                    "Total_ST_Population": row["p_st"],
                    "Male_ST_Population": row["m_st"],
                    "Female_ST_Population": row["f_st"],
                    "Total_Literate_Population": row["p_lit"],
                    "Male_Literate_Population": row["m_lit"],
                    "Female_Literate_Population": row["f_lit"],
                    "Total_Illiterate_Population": row["p_ill"],
                    "Male_Illiterate_Population": row["m_ill"],
                    "Female_Illiterate_Population": row["f_ill"],
                },
            )
        )

    # Create the directory for state if doesn't exist already
    state_dir = os.path.join(SHAPE_FILES_OUTPUT_DIR, state_name)

    if not os.path.exists(state_dir):
        os.mkdir(state_dir)

    # Creating the feature collection out of the features list built in the previous cell
    collection = FeatureCollection(features)

    path = os.path.join(state_dir, f"{district_name}_{block_name}")
    # Write the feature collection into json file
    with open(path + ".json", "w") as f:
        try:
            f.write(f"{collection}")
        except Exception as e:
            print(e)

    path = generate_shape_files(path)
    return push_shape_to_geoserver(path)


def kml_to_geojson(state_name, district_name, block_name, kml_path):
    fiona.drvsupport.supported_drivers[
        "kml"
    ] = "rw"  # enable KML support which is disabled by default
    fiona.drvsupport.supported_drivers[
        "KML"
    ] = "rw"  # enable KML support which is disabled by default
    gdf = gpd.read_file(kml_path)
    geometry_types = gdf.geometry.geometry.type.unique()
    state_dir = os.path.join(SHAPE_FILES_OUTPUT_DIR, state_name)

    for gtype in geometry_types:
        df = gdf.loc[gdf.geometry.geometry.type == gtype]
        path = os.path.join(state_dir, f"{district_name}_{block_name}_{gtype}")
        df.to_file(path + ".json", driver="GeoJSON")
        generate_shape_files(path)
        push_shape_to_geoserver(path)

    print("Deleting kml file at path::" + kml_path)
    os.remove(kml_path)
