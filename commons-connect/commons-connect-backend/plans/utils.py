import requests
import json
from nrm_app.settings import OD_DATA_URL_hemlet, ODK_PASSWORD, ODK_USERNAME, OD_DATA_URL_well, OD_DATA_URL_wb, OD_DATA_URL_plan, ASSET_DIR
import csv
from geojson import Feature, FeatureCollection, Polygon
features = []
from collections import OrderedDict
import json
from shapely import wkt
import pandas as pd
import geopandas as gpd
from shapely.geometry import Point
import os
import shutil
from django.conf import settings
from datetime import datetime
import json


def generate_output_dir(block_name, layer_name):
    zip_dir= ASSET_DIR+str(block_name)+'_'+str(layer_name)
    if os.path.exists(zip_dir):
       print ('dir exists')
       try:
         shutil.rmtree(zip_dir)
       except Exception as e:
           print (e)
       os.mkdir(zip_dir)
    else:
        print ("creating dir")
        os.mkdir(zip_dir)

    output_file_geojson = zip_dir+'/'+str(layer_name)+str(block_name)+".geojson"
    output_shape_file = zip_dir+'/'+str(layer_name)+str(block_name)+".shp"
    return zip_dir, output_file_geojson, output_shape_file


def modify_response_list_well(rls):
    response_list = []
    print (rls)
    for rl in rls:
        print (rl)
        if rl["GPS_point_well"]:
            rl["lat"] = rl["GPS_point_well"]["point_mapappearance"]["coordinates"][0]
            rl["long"] = rl["GPS_point_well"]["point_mapappearance"]["coordinates"][1]
            response_list.append(rl)
        rl["status"] = (rl["__system"]["reviewState"])
        response_list.append(rl)
    return response_list


def modify_response_list_hemlet(results):
    response_list = []
    for result in results:
        print (result)
        if result["GPS_point"]:
            try:      
               result["lat"] = result["GPS_point"]["point_mapsappearance"]["coordinates"][0]
               result["long"] = result["GPS_point"]["point_mapsappearance"]["coordinates"][1]
               response_list.append(result)
            except  Exception as e:
                continue
        result["status"] = (result["__system"]["reviewState"])
        response_list.append(result)
    return response_list



def modify_response_list_plan(results, gps_point):
    response_list = []
    for result in results:
        try: 
        
          if result[gps_point]:
             print (result)
             try:
                result["lat"] = result[gps_point]["point_mapsappearance"]["coordinates"][0]
                result["long"] = result[gps_point]["point_mapsappearance"]["coordinates"][1]
                
             except Exception as e:
                continue
        except Exception as e:
          continue
        result["status"] = (result["__system"]["reviewState"])
        response_list.append(result)
    return response_list


def fetch_odk_result(layer_name, csv_path, layer_type=None):

    print (layer_name) 
    if layer_name == "hemlet_layer":
        response_obj = requests.get(OD_DATA_URL_hemlet, auth=(ODK_USERNAME, ODK_PASSWORD))
        response_dict=json.loads(response_obj.content)
        response_list = response_dict["value"]
  
        res_list = modify_response_list_hemlet(response_list)
        header_keys = res_list[0].keys()
        with open(csv_path, 'w') as output_file:
            dict_writer = csv.DictWriter(output_file, header_keys)
            dict_writer.writeheader()
            dict_writer.writerows(res_list)

    if layer_name == "well_layer":
        response_obj = requests.get(OD_DATA_URL_well, auth=(ODK_USERNAME, ODK_PASSWORD))
        response_dict=json.loads(response_obj.content)
        response_list = response_dict["value"]
        res_list = modify_response_list_well(response_list)
        header_keys = res_list[0].keys()
        with open(csv_path, 'w') as output_file:
            dict_writer = csv.DictWriter(output_file, header_keys)
            dict_writer.writeheader()
            dict_writer.writerows(res_list)


    if layer_name == "wb_layer":
        response_obj = requests.get(OD_DATA_URL_wb, auth=(ODK_USERNAME, ODK_PASSWORD))
        response_dict=json.loads(response_obj.content)
        response_list = response_dict["value"]
        res_list = modify_response_list_well(response_list)
        header_keys = res_list[0].keys()
        with open(csv_path, 'w') as output_file:
            dict_writer = csv.DictWriter(output_file, header_keys)
            dict_writer.writeheader()
            dict_writer.writerows(res_list)

    if layer_name in ["plan_layer_wb", "plan_layer_agri", "plan_layer_gw"]:
        response_obj = requests.get(OD_DATA_URL_plan["odk_prop_"+layer_type]["odk_url"], auth=(ODK_USERNAME, ODK_PASSWORD))
        print (response_obj.content)
        response_dict=json.loads(response_obj.content)
        response_list = response_dict["value"]
        res_list = modify_response_list_plan(response_list, OD_DATA_URL_plan["odk_prop_"+layer_type]["gps_point"])
        print ("res_list")
        print (res_list)
        res_list[0]['Trench_cum_bunds'] = None
        res_list[0]['Canal_repair'] = None
        res_list[0]['Check_dam'] = None
        res_list[0]['Farm_pond'] = None
        res_list[0]['Land_leveling'] = None
        res_list[0]['Fishery'] = None
        header_keys = res_list[0].keys()
        print ('csv:'+csv_path)
        with open(csv_path, 'w') as output_file:
            dict_writer = csv.DictWriter(output_file, header_keys)
            dict_writer.writeheader()
            dict_writer.writerows(res_list)



def build_shape_file_hemlet(response_list, input_block_name,output_file_geojson, csv_path, output_shapefile):
    print (output_file_geojson)
    print (output_shapefile)
    df_shapes = pd.read_csv(csv_path)
    df_shapes.reset_index(inplace=True)
    for xy in zip(df_shapes['lat'], df_shapes['long']):
        print (xy)
    geometry_path = [Point(xy) for xy in zip(df_shapes['lat'], df_shapes['long'])]



    print (geometry_path)
    df_shapes = gpd.GeoDataFrame(df_shapes, crs="EPSG:4326", geometry=geometry_path)
    print (df_shapes)
    print ("here")
    features = []
    for index_sq, row in df_shapes.iterrows():
        print (row)
        totnohh = df_shapes.loc[index_sq, 'number_households']
        mod_status = df_shapes.loc[index_sq, 'status']
        hamlet_id = df_shapes.loc[index_sq, 'hamlet_id']
        block_name = df_shapes.loc[index_sq, 'block_name']
        print (str(input_block_name))
        print (str(mod_status))
        if str(block_name).lower() == input_block_name and mod_status != 'rejected' :
            print ("-------Inside for loop----------------")
            print ("block_name:" +str(block_name.lower()))
            print ("input name:" +str(input_block_name.lower()))
            features.append(
                Feature(
                geometry = df_shapes.loc[index_sq, 'geometry'],
                 properties = {
                    'hemlet_id': hamlet_id,
                    'number_households':str(totnohh),
                    'status':str(mod_status),
                    'block_name':str(block_name),
                    
                    }
                )
            )

    print(len(features))
    collection = FeatureCollection(features)
    d = OrderedDict()
    d['type'] = 'FeatureCollection'
    d['features'] = features
    with open(output_file_geojson,'w') as f:
       json.dump(d,f,indent=2)



def build_shape_file_well(response_list, input_block_name,output_file_geojson, csv_path, output_shapefile):
    df_shapes = pd.read_csv(csv_path)
    df_shapes.reset_index(inplace=True)
   
    geometry_path = [Point(xy) for xy in zip(df_shapes['lat'], df_shapes['long'])]



    print (geometry_path)
    df_shapes = gpd.GeoDataFrame(df_shapes, crs="EPSG:4326", geometry=geometry_path)
    print (df_shapes)
    features = []
    for index_sq, row in df_shapes.iterrows():

        totnohh = df_shapes.loc[index_sq, 'block_name']
        well_id = df_shapes.loc[index_sq, 'well_id']
        mod_status = df_shapes.loc[index_sq, 'status']

        if str(totnohh).lower() == input_block_name and str(mod_status).lower() != 'rejected':
           features.append(
                 Feature(
                    geometry = df_shapes.loc[index_sq, 'geometry'],
                    properties = {

                       'block_name':str(totnohh),
                    '   well_id':str(well_id)
                   }
               )
            )

    print(len(features))
    collection = FeatureCollection(features)
    d = OrderedDict()
    d['type'] = 'FeatureCollection'
    d['features'] = features
    with open(output_file_geojson,'w') as f:
       json.dump(d,f,indent=2)


def build_shape_file_wb(response_list, input_block_name,output_file_geojson, csv_path, output_shapefile):
    df_shapes = pd.read_csv(csv_path)
    df_shapes.reset_index(inplace=True)
    geometry_path = [Point(xy) for xy in zip(df_shapes['lat'], df_shapes['long'])]



    print (geometry_path)
    df_shapes = gpd.GeoDataFrame(df_shapes, crs="EPSG:4326", geometry=geometry_path)
    print (df_shapes)
    features = []
    for index_sq, row in df_shapes.iterrows():

        totnohh = df_shapes.loc[index_sq, 'block_name']
        waterbodies_id = df_shapes.loc[index_sq, 'waterbodies_id']
        mod_status = df_shapes.loc[index_sq, 'status']
        
        print (totnohh);
        if str(totnohh).lower() == input_block_name and mod_status != 'rejected':

           print ('adding feature')
           features.append(
                 Feature(
                    geometry = df_shapes.loc[index_sq, 'geometry'],
                    properties = {

                    'block_name':str(totnohh),
                    'waterbodies_id':str(waterbodies_id)
                   }
               )
            )

    print(len(features))
    collection = FeatureCollection(features)
    d = OrderedDict()
    d['type'] = 'FeatureCollection'
    d['features'] = features
    with open(output_file_geojson,'w') as f:
       json.dump(d,f,indent=2)

def getGeometryPath(df_shapes):
    geo_list = []
    for xy in zip(df_shapes['lat'], df_shapes['long']):
        try:
            geo_list.append(Point(xy))
        except Exception as e:
            geo_list.append(Point(('62.121328', '104.861060')))
    print (geo_list)
    return geo_list


def row_converter(row, listy):
    print (listy)
    #convert pandas row to a dictionary
    #requires a list of columns and a row as a tuple
    count = 1
    pictionary = {}
    for item in listy:
        try:
           pictionary[item] = row[count]
        except Exception as e:
           pictionary[item] = None
        count += 1
    print(pictionary)
    return pictionary

def getProperties(layer_type, row):
    prop = {}
    if layer_type == "wb":
        prop["date"] = str(row["start"])
        prop["block_name"] = str(row["block_name"])
        prop["work_id"] = str(row["work_id"])
        prop["type of work"] = str(row["TYPE_OF_WORK_ID"])
        prop["status"] = str(row["status"])
        prop["work_type"] = "wb"
    if layer_type == "agri":
        prop["date"] = str(row["today"])
        prop["block_name"] = str(row["block_name"])
        prop["work_id"] = str(row["work_id"])
        prop["type of work"] = str(row["TYPE_OF_WORK_ID"])
        prop["status"] = str(row["status"])
        prop["famer_name"] = str(row["famer_name_id"])
        prop["work_type"] = "agri"
    if layer_type == "gw":
        prop["date"] = str(row["today"])
        prop["block_name"] = str(row["block_name"])
        prop["work_id"] = str(row["work_id"])
        prop["type of work"] = str(row["TYPE_OF_WORK_ID"])
        prop["status"] = str(row["status"])

        prop["work_type"] = "gw"



    return prop


def getFeatures(df_shapes, layer_type, input_block_name):
    print ('layer type: '+layer_type)
    geometry_path = [Point(xy) for xy in zip(df_shapes['lat'], df_shapes['long'])]
    features = []
    print (geometry_path)
    df_shapes = gpd.GeoDataFrame(df_shapes, crs="EPSG:4326", geometry=geometry_path)
    print ('--------------------------')
    for index_sq, row in df_shapes.iterrows():
       
       prop_dict = {}
       feature_dict = row.to_dict()
       print (feature_dict)
       prop = getProperties(layer_type, feature_dict)
       print (prop)
           
       block_name = df_shapes.loc[index_sq, 'block_name']
       status = df_shapes.loc[index_sq, 'status']
       if str(block_name).lower() == input_block_name.lower() and status != 'rejected':
           feature_dict = row
           features.append(
                 Feature(
                    geometry = df_shapes.loc[index_sq, 'geometry'],
                    properties = prop
               )
            )
    return features
           
           
    
def build_shape_file_plan(response_list, input_block_name,output_file_geojson, csv_path, output_shapefile, layer_type):
    df_shapes = pd.read_csv(csv_path)
    df_shapes.reset_index(inplace=True)
    features = getFeatures(df_shapes, layer_type, input_block_name)
    print("exit loop")
    collection = FeatureCollection(features)
    d = OrderedDict()
    d['type'] = 'FeatureCollection'
    d['features'] = features
    print ('here'+output_file_geojson)
    with open(output_file_geojson,'w') as f:
       json.dump(d,f,indent=2)

                                                    