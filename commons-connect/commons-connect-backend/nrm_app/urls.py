"""
URL configuration for nrm_app project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from plans.views import updateHemletLayer
from computing.api import (
    generate_block_layer,
    create_workspace,
    delete_layer,
    upload_kml
)
from .api import GetStatesAPI, GetDistrictsAPI, GetBlocksAPI

urlpatterns = [
    path('admin/', admin.site.urls),
    path('post/', updateHemletLayer),
    path('v1/create_workspace', create_workspace),
    path('v1/generate_block_layer', generate_block_layer),
    path('v1/delete_layer', delete_layer),
    path('v1/upload_kml', upload_kml),
    path('api/v1/get_states/', GetStatesAPI.as_view(), name='get_states'),
    path('api/v1/get_districts/<str:state_name>/', GetDistrictsAPI.as_view(), name='get_districts'),
    path('api/v1/get_blocks/<str:state_name>/<str:district_name>/', GetBlocksAPI.as_view(), name='get_blocks'),
]
