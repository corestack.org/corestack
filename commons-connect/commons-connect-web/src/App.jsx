import Header from './components/header';
import MapArea from './components/mapArea';

function App() {
  return (
    <div className='overflow-y-hidden'>
        <Header/>
        <MapArea/>
    </div>
  );
}

export default App;
