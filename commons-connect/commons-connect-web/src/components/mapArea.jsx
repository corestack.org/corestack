import { useEffect, useRef, useState } from "react";

import "ol/ol.css";
import { Icon, Style, Stroke, Fill } from "ol/style.js";

import XYZ from "ol/source/XYZ";
import { Map, View, Feature } from "ol";
import Point from "ol/geom/Point.js";
import VectorLayer from "ol/layer/Vector";
import TileLayer from "ol/layer/Tile";
import VectorSource from "ol/source/Vector";

import GeoJSON from "ol/format/GeoJSON";
import KML from "ol/format/KML.js";

import getStates from "../actions/getStates";
import getPlans from "../actions/getPlans";
import getVectorLayers from "../actions/getVectorLayers";
import getImageLayer from "../actions/getImageLayers";

import mapMarker from "../assets/map_marker.svg";
import well_mrker from "../assets/well_proposed.svg";
import wb_mrker from "../assets/waterbodies_proposed.svg";
import settlementIcon from "../assets/settlement_icon.svg";
import tcb_proposed from "../assets/tcb_proposed.svg";
import boulder_proposed from "../assets/boulder_proposed.svg";
import farm_pond_proposed from "../assets/farm_pond_proposed.svg";
import check_dam_proposed from "../assets/check_dam_proposed.svg";
import land_leveling_proposed from "../assets/land_leveling_proposed.svg";

//? Temprorary Imports


const MapArea = () => {
  let mapElement = useRef(null);
  let mapRef = useRef(null);

  //? Layers Ref Group
  let LayersArray = [
    {LayerRef : useRef(null), name : "Panchayat Boundaries"},
    {LayerRef : useRef(null), name : "Drainage Layer"},
    {LayerRef : useRef(null), name : "Remote-Sensed Waterbodies Layer"},
    {LayerRef : useRef(null), name : "Well Depth Layer"},
    {LayerRef : useRef(null), name : "Clart Layer"},
  ]
  let PlanLayersArray = [
    {LayerRef : useRef(null), name : "Settlement Layer"},
    {LayerRef : useRef(null), name : "Water Structure Layer"},
    {LayerRef : useRef(null), name : "Agristructure Layer"},
  ]

  let markersLayer = useRef(null);

  const [stateData, setStateData] = useState(null);

  //? States for handling the Selectiion of states, district and block
  const [state, setState] = useState(null);
  const [district, setDistrict] = useState(null);
  const [block, setBlock] = useState(null);

  //? States for Handling the plan selection
  const [plans, setPlans] = useState(null);
  const [selectedPlan, setSelectedPlan] = useState(null);

  //? Dropdown Toggle Group
  const [stateToggle, setStateToggle] = useState(false);
  const [districtToggle, setDistrictToggle] = useState(false);
  const [blockToggle, setBlockToggle] = useState(false);
  const [planToggle, setPlanToggle] = useState(false);

  //? Layers Present
  const [isLayersFetched, setIsLayersFetched] = useState(false)
  const [isOtherLayersFetched, setIsOtherLayersFetched] = useState(false);

  //? Miscellaneous States and Variables
  const [isLoading, setIsLoading] = useState(false);
  const [isLayersLoading, setIsLayersLoading] = useState(false);
  const [hrefData, setHrefData] = useState(null);
  const drainageColors = [
    "03045E",
    "023E8A",
    "0077B6",
    "0096C7",
    "00B4D8",
    "48CAE4",
    "90E0EF",
    "ADE8F4",
    "CAF0F8",
  ];
  const data = [
    {
        label: "2017-2022",
        value: "2017",
    },
    {
        label: "2018-2023",
        value: "2018",
    },
  ];
  const [bbox, setBBox] = useState(null);

  function changePolygonColor(color) {
    return new Style({
        fill: new Fill({
            color: color,
            opacity: 0.4,
        }),
        stroke: new Stroke({
            color: "transparent",
            width: 0,
        }),
    });
  }

  const getblockFeatures = async (block_name) => {
    let coordinates = null;

    let AdminURl = `https://geoserver.gramvaani.org:8443/geoserver/panchayat_boundaries/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=panchayat_boundaries:${block_name.toLowerCase()}_panchayat&outputFormat=application/json&screen=main`;

    try {
      await fetch(AdminURl)
        .then((res) => res.json())
        .then((Geojson) => {
          coordinates = Geojson.features[0].geometry.coordinates[0][0][0];
        });
    } catch (e) {
      console.log("error in fetching for : ", block_name);
      return [];
    }
    return coordinates;
  };

  const getStatesData = async () => {
    let data = await getStates();

    setStateData(data)

    let temp_blockNames = [];

    data.map((item) => {
      temp_blockNames.push(item.district[0].blocks[0].label);
    });

    let layer_features = await Promise.all(
      temp_blockNames.map((item) => {
        return getblockFeatures(item);
      })
    ).then((res) => {
      return res.map(
        (coord) =>
          new Feature({
            geometry: new Point(coord),
          })
      );
    });

    const StateMarkers = new Style({
      image: new Icon({
        src: mapMarker,
      }),
    });

    let StateLevelLayer = new VectorLayer({
      source: new VectorSource({
        features: layer_features,
      }),
      style: StateMarkers,
    });

    mapRef.current.addLayer(StateLevelLayer);

    markersLayer.current = StateLevelLayer;
  };

  const getDistrictData = async () => {
    let temp_blocks = state.district.map((item) => {
      return item.blocks[0].label;
    });

    let temp_coordinates = null;

    let layer_features = await Promise.all(
      temp_blocks.map((item) => {
        return getblockFeatures(item);
      })
    ).then((res) => {
      return res.map((coord) => {
        if (temp_coordinates == null) temp_coordinates = coord;

        return new Feature({
          geometry: new Point(coord),
        });
      });
    });

    const StateMarkers = new Style({
      image: new Icon({
        src: mapMarker,
      }),
    });

    let districtLevelLayer = new VectorLayer({
      source: new VectorSource({
        features: layer_features,
      }),
      style: StateMarkers,
    });

    mapRef.current.removeLayer(markersLayer.current);

    mapRef.current.addLayer(districtLevelLayer);

    markersLayer.current = districtLevelLayer;

    mapRef.current.getView().setCenter(temp_coordinates);
    mapRef.current.getView().setZoom(8);
  };

  const getBlockData = async () => {
    const temp_blocks = district.blocks.map((item) => {
      return item.label;
    });

    let temp_coordinates = null;

    //console.log(temp_blocks)

    let layer_features = await Promise.all(
      temp_blocks.map((item) => {
        console.log(item);
        return getblockFeatures(item);
      })
    ).then((res) => {
      return res.map((coord) => {
        if (temp_coordinates == null) temp_coordinates = coord;

        return new Feature({
          geometry: new Point(coord),
        });
      });
    });

    const StateMarkers = new Style({
      image: new Icon({
        src: mapMarker,
      }),
    });

    let blockLevelLayer = new VectorLayer({
      source: new VectorSource({
        features: layer_features,
      }),
      style: StateMarkers,
    });

    mapRef.current.removeLayer(markersLayer.current);

    mapRef.current.addLayer(blockLevelLayer);

    markersLayer.current = blockLevelLayer;

    mapRef.current.getView().setCenter(temp_coordinates);
    mapRef.current.getView().setZoom(11);
  };

  useEffect(() => {
    getStatesData();

    let BaseLayer = new TileLayer({
      source: new XYZ({
        url: "https://www.google.cn/maps/vt?lyrs=s@189&gl=cn&x={x}&y={y}&z={z}",
        maxZoom: 30,
      }),
      visible: true,
    });

    const view = new View({
      center: [78.9, 20.5],
      zoom: 5,
      projection: "EPSG:4326",
    });

    const initialMap = new Map({
      target: mapElement.current,
      layers: [BaseLayer],
      view: view,
    });

    mapRef.current = initialMap;

    return () => {
      initialMap.setTarget(null);
    };
  }, []);

  useEffect(() => {
    if (state != null) {
      getDistrictData();
    }
    if (district != null) {
      getBlockData();
    }
  }, [state, district]);

  const fetchLayers = async () => {
    if (selectedPlan != null) {
      setIsLayersLoading(true)
      //? Code for settlement Layer
      let settlementLayer = await getVectorLayers(
        "resources",
        "hemlet_layer" + block.label.toLowerCase(),
        true,
        true,
        "settlement",
        selectedPlan.plan_id,
        district.label.toLowerCase(),
        block.label.toLowerCase()
      );

      if (PlanLayersArray[0].LayerRef.current != null) {
        mapRef.current.removeLayer(PlanLayersArray[0].LayerRef.current);
      }

      mapRef.current.addLayer(settlementLayer);

      PlanLayersArray[0].LayerRef.current = settlementLayer;

      PlanLayersArray[0].LayerRef.current.setStyle(
        new Style({
          image: new Icon({ src: settlementIcon }),
        })
      );

      //? Code For Water Structures Layer
      let WaterStructuresLayer = await getVectorLayers(
        "works",
        "plan_layer_gw" + block.label.toLowerCase(),
        true,
        false,
        "plan_gw",
        selectedPlan.plan_id,
        district.label.toLowerCase(),
        block.label.toLowerCase()
      );

      if (PlanLayersArray[1].LayerRef.current != null) {
        mapRef.current.removeLayer(PlanLayersArray[1].LayerRef.current);
      }

      mapRef.current.addLayer(WaterStructuresLayer);

      PlanLayersArray[1].LayerRef.current = WaterStructuresLayer;

      PlanLayersArray[1].LayerRef.current.setStyle(function (feature) {
        const status = feature.values_;

        if (status.work_type == "new farm pond") {
          return new Style({
            image: new Icon({ src: farm_pond_proposed }),
          });
        } else if (status.work_type == "new trench cum bund network") {
          return new Style({
            image: new Icon({ src: tcb_proposed }),
          });
        } else if (status.work_type == "new check dam") {
          return new Style({
            image: new Icon({ src: check_dam_proposed }),
          });
        } else {
          return new Style({
            image: new Icon({ src: boulder_proposed }),
          });
        }
      });

      //? Code for Agri Structures Layer
      let AgriStructuresLayer = await getVectorLayers(
        "works",
        "plan_layer_agri" + block.label.toLowerCase(),
        true,
        false,
        "plan_agri",
        selectedPlan.plan_id,
        district.label.toLowerCase(),
        block.label.toLowerCase()
      );

      if (PlanLayersArray[2].LayerRef.current != null) {
        mapRef.current.removeLayer(PlanLayersArray[2].LayerRef.current);
      }

      mapRef.current.addLayer(AgriStructuresLayer);
      PlanLayersArray[2].LayerRef.current = AgriStructuresLayer;

      PlanLayersArray[2].LayerRef.current.setStyle(function (feature) {
        const status = feature.values_;

        if (status.TYPE_OF_WO == "New farm pond") {
          return new Style({
            image: new Icon({ src: farm_pond_proposed }),
          });
        } else if (status.TYPE_OF_WO == "Land leveling") {
          return new Style({
            image: new Icon({ src: land_leveling_proposed }),
          });
        } else if (status.TYPE_OF_WO == "New well") {
          return new Style({
            image: new Icon({ src: well_mrker }),
          });
        } else {
          return new Style({
            image: new Icon({ src: wb_mrker }),
          });
        }
      });

      setIsLayersLoading(false)
      setIsOtherLayersFetched(true)
    }
  };

  const handleLocationChange = async () => {
    if (block != null) {
      setIsLoading(true);

      let temp_plan = await getPlans(block.block_id);

      setPlans(temp_plan);

      let adminLayer = await getVectorLayers(
        "panchayat_boundaries",
        block.label.toLowerCase() + "_panchayat",
        true,
        true
      );

      //? Remove the admin Layer if exists to display only one admin layer at a time
      if (LayersArray[0].LayerRef.current != null) {
        mapRef.current.removeLayer(LayersArray[1].LayerRef.current);
      }

      mapRef.current.addLayer(adminLayer);

      LayersArray[0].LayerRef.current = adminLayer;

      //? Centering the Map to the Selected Block
      const Vectorsource = adminLayer.getSource();
      Vectorsource.once("change", function (e) {
        if (Vectorsource.getState() === "ready") {
          const arr = Vectorsource.getExtent();
          setBBox(arr);
          const mapcenter = [(arr[0] + arr[2]) / 2, (arr[1] + arr[3]) / 2];
          mapRef.current.getView().setCenter(mapcenter);
          mapRef.current.getView().setZoom(11);
        }
      });

      //? Remove the Marker Layer to display only the Plan layer
      mapRef.current.removeLayer(markersLayer.current);

      //? Code For Drainage Layer
      let DrainageLayer = await getVectorLayers(
        "drainage",
        block.label.toLowerCase() + "_drainage_lines",
        true,
        true,
        "drainage",
        0,
        district.label.toLowerCase(),
        block.label.toLowerCase()
      );

      DrainageLayer.setStyle(function (feature) {
        let order = feature.values_.ORDER;

        return new Style({
          stroke: new Stroke({
            color: `#${drainageColors[order - 1]}`,
            width: 2.0,
          }),
        });
      });

      if (LayersArray[1].LayerRef.current != null) {
        mapRef.current.removeLayer(LayersArray[1].LayerRef.current);
      }

      mapRef.current.addLayer(DrainageLayer);
      LayersArray[1].LayerRef.current = DrainageLayer;

      //? Code for Remote Sensed Waterbodies Layer

      let RemoteSensedWaterbodiesLayer = await getVectorLayers(
        "water_bodies",
        "surface_waterbodies_" +
        block.label.toLowerCase(),
        true,
        true,
        "Water_bodies",
        null,
        district.label.toLowerCase(),
        block.label.toLowerCase()
      );

      RemoteSensedWaterbodiesLayer.setStyle(
        new Style({
          stroke: new Stroke({ color: "#6495ed", width: 5 }),
          fill: new Fill({ color: "rgba(100, 149, 237, 0.5)" }),
        })
      );

      if (LayersArray[2].LayerRef.current != null) {
        mapRef.current.removeLayer(LayersArray[2].LayerRef.current);
      }

      mapRef.current.addLayer(RemoteSensedWaterbodiesLayer);
      LayersArray[2].LayerRef.current = RemoteSensedWaterbodiesLayer;

      //? Code For MicroWatershed Layer

      let MicroWaterShedLayer = await getVectorLayers(
        "mws_layers",
        "deltaG_well_depth_" + block.label.toLowerCase(),
        true,
        true,
        "Watershed",
        null,
        district.label.toLowerCase(),
        block.label.toLowerCase()
      )

      if (LayersArray[3].LayerRef.current != null) {
        mapRef.current.removeLayer(LayersArray[3].LayerRef.current);
      }

      MicroWaterShedLayer.getSource().forEachFeature(function(feature) {
        //const year_value = data["2017"].value;
        const year_value = "2017"
        let bin;
        if (year_value == 2017) {
            bin = feature.values_["Net" + year_value + "_22"];
        } else if (year_value == 2018) {
            bin = feature.values_["Net" + year_value + "_23"];
        }

        if (bin < -5) {
            feature.setStyle(changePolygonColor("rgba(255, 0, 0, 0.5)")); // red
        } else if (bin >= -5 && bin < -1) {
            feature.setStyle(changePolygonColor("rgba(255, 255, 0, 0.5)")); // yellow
        } else if (bin >= -1 && bin <= 1) {
            feature.setStyle(changePolygonColor("rgba(0, 255, 0, 0.5)")); // green
        } else if (bin > 1) {
            feature.setStyle(changePolygonColor("rgba(0, 0, 255, 0.5)")); // blue
        }
      });

      mapRef.current.addLayer(MicroWaterShedLayer);
      LayersArray[3].LayerRef.current = MicroWaterShedLayer;

      //? Code For Clart Layer
      let clartLayer = await getImageLayer(
        "clart",
        "CLART_" +  block.label.toLowerCase()
      );
      
      mapRef.current.addLayer(clartLayer);
      LayersArray[4].LayerRef.current = clartLayer;

      setIsLoading(false);
      setIsLayersFetched(true)
    }
  };

  const handleMenuToggle = (setter, state) => {
    setter(!state);
  };

  const handleItemSelect = (setter, state, toggle) => {
    setter(state);
    toggle(false);
  };
  
  const handleLayerButtonToggle = (LayerObj) => {
    LayerObj.LayerRef.current.setVisible(!LayerObj.LayerRef.current.getVisible())
  };

  const handleGeoJsonLayers = (layerRef) => {
    if (layerRef.current != null) {
      const format = new GeoJSON({ featureProjection: "EPSG:4326" });

      const layerSource = layerRef.current.getSource();
      const features = layerSource.getFeatures();
      const json = format.writeFeatures(features);

      setHrefData(
        "data:application/json;charset=utf-8," + encodeURIComponent(json)
      );
    }
  };

  const handleKMLLayers = (layerRef) => {
    if (layerRef.current != null) {
      const format = new KML({ featureProjection: "EPSG:4326" });

      const layerSource = layerRef.current.getSource();
      const features = layerSource.getFeatures();
      const kmlData = format.writeFeatures(features);

      setHrefData(
        "data:application/kml;charset=utf-8," + encodeURIComponent(kmlData)
      );
    }
  };

  const handleImageLayers = () => {
    if(bbox !== null){
      const downloadurl = `https://geoserver.gramvaani.org:8443/geoserver/clart/wms?service=WMS&version=1.1.0&request=GetMap&layers=clart%3ACLART_${block.label.toLowerCase()}&bbox=${bbox[0]}%2C${bbox[1]}%2C${bbox[2]}%2C${bbox[3]}&width=800&height=750&srs=EPSG%3A4326&styles=&format=image%2Fgeotiff`
      window.open(downloadurl);
    }
    else{
      console.log("Wait !")
    }
  }

  return (
    <>
        <div className="ml-10 my-24 mx-auto flex sm:flex-nowrap flex-wrap">
          <div className="h-[80vh] lg:w-5/6 md:w-1/2 bg-gray-300 rounded-lg inset-0 sm:mr-10 flex items-end justify-start ">
            <div ref={mapElement} className="h-full w-full" />
          </div>
          <div className="lg:w-1/4 bg-white flex flex-col items-center mr-2 w-full h-[80vh] overflow-y-auto overflow-x-hidden">
            <h2 className="text-gray-900 text-md mb-1 font-medium title-font text-center">
              Select Location
            </h2>

            <div className="relative mb-4 w-4/5">
              <button
                id="dropdown-button"
                onClick={() => handleMenuToggle(setStateToggle, stateToggle)}
                className="inline-flex justify-center w-full px-4 py-2 text-sm font-medium text-gray-700 bg-white border border-gray-300 rounded-md shadow-sm focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-100 focus:ring-blue-500"
              >
                <span className="mr-2">
                  {state == null ? "Select State" : state.label}
                </span>
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="w-5 h-5 ml-2 -mr-1"
                  viewBox="0 0 20 20"
                  fill="currentColor"
                  aria-hidden="true"
                >
                  <path
                    fillRule="evenodd"
                    d="M6.293 9.293a1 1 0 011.414 0L10 11.586l2.293-2.293a1 1 0 111.414 1.414l-3 3a1 1 0 01-1.414 0l-3-3a1 1 0 010-1.414z"
                    clipRule="evenodd"
                  />
                </svg>
              </button>
              <div
                id="dropdown-menu"
                className={`${
                  stateToggle ? "block" : "hidden"
                } z-10 absolute right-0 mt-2 rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5 p-1 space-y-1 h-32 overflow-auto`}
              >
                <input
                  id="search-input"
                  className="block w-full px-4 py-2 text-gray-800 border rounded-md  border-gray-300 focus:outline-none"
                  type="text"
                  placeholder="Search items"
                  autoComplete="off"
                />

                {stateData != null &&
                  stateData.map((item, idx) => {
                    return (
                      <>
                        <a
                          href="#"
                          key={idx}
                          className="block px-4 py-2 text-gray-700 hover:bg-gray-100 active:bg-blue-100 cursor-pointer rounded-md"
                          onClick={() =>
                            handleItemSelect(setState, item, setStateToggle)
                          }
                        >
                          {item.label}
                        </a>
                      </>
                    );
                  })}
              </div>
            </div>
            <div className="relative mb-4 w-4/5">
              <button
                id="dropdown-button"
                onClick={() =>
                  handleMenuToggle(setDistrictToggle, districtToggle)
                }
                className="inline-flex justify-center w-full px-4 py-2 text-sm font-medium text-gray-700 bg-white border border-gray-300 rounded-md shadow-sm focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-100 focus:ring-blue-500"
              >
                <span className="mr-2">
                  {district == null ? "Select District" : district.label}
                </span>
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="w-5 h-5 ml-2 -mr-1"
                  viewBox="0 0 20 20"
                  fill="currentColor"
                  aria-hidden="true"
                >
                  <path
                    fillRule="evenodd"
                    d="M6.293 9.293a1 1 0 011.414 0L10 11.586l2.293-2.293a1 1 0 111.414 1.414l-3 3a1 1 0 01-1.414 0l-3-3a1 1 0 010-1.414z"
                    clipRule="evenodd"
                  />
                </svg>
              </button>
              <div
                id="dropdown-menu"
                className={`${
                  districtToggle && state != null ? "block" : "hidden"
                } z-10 absolute right-0 mt-2 rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5 p-1 space-y-1 h-32 overflow-auto`}
              >
                <input
                  id="search-input"
                  className="block w-full px-4 py-2 text-gray-800 border rounded-md  border-gray-300 focus:outline-none"
                  type="text"
                  placeholder="Search items"
                  autoComplete="off"
                />

                {state != null &&
                  state.district.map((item, idx) => {
                    return (
                      <>
                        <a
                          href="#"
                          key={idx}
                          className="block px-4 py-2 text-gray-700 hover:bg-gray-100 active:bg-blue-100 cursor-pointer rounded-md"
                          onClick={() =>
                            handleItemSelect(
                              setDistrict,
                              item,
                              setDistrictToggle
                            )
                          }
                        >
                          {item.label}
                        </a>
                      </>
                    );
                  })}
              </div>
            </div>
            <div className="relative mb-4 w-4/5">
              <button
                id="dropdown-button"
                onClick={() => handleMenuToggle(setBlockToggle, blockToggle)}
                className="inline-flex justify-center w-full px-4 py-2 text-sm font-medium text-gray-700 bg-white border border-gray-300 rounded-md shadow-sm focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-100 focus:ring-blue-500"
              >
                <span className="mr-2">
                  {block == null ? "Select Block" : block.label}
                </span>
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="w-5 h-5 ml-2 -mr-1"
                  viewBox="0 0 20 20"
                  fill="currentColor"
                  aria-hidden="true"
                >
                  <path
                    fillRule="evenodd"
                    d="M6.293 9.293a1 1 0 011.414 0L10 11.586l2.293-2.293a1 1 0 111.414 1.414l-3 3a1 1 0 01-1.414 0l-3-3a1 1 0 010-1.414z"
                    clipRule="evenodd"
                  />
                </svg>
              </button>
              <div
                id="dropdown-menu"
                className={`${
                  blockToggle && district != null ? "block" : "hidden"
                } z-10 absolute right-0 mt-2 rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5 p-1 space-y-1 h-32 overflow-auto`}
              >
                <input
                  id="search-input"
                  className="block w-full px-4 py-2 text-gray-800 border rounded-md  border-gray-300 focus:outline-none"
                  type="text"
                  placeholder="Search items"
                  autoComplete="off"
                />

                {district != null &&
                  district.blocks.map((items, idx) => {
                    return (
                      <>
                        <a
                          href="#"
                          key={idx}
                          className="block px-4 py-2 text-gray-700 hover:bg-gray-100 active:bg-blue-100 cursor-pointer rounded-md"
                          onClick={() =>
                            handleItemSelect(setBlock, items, setBlockToggle)
                          }
                        >
                          {items.label}
                        </a>
                      </>
                    );
                  })}
              </div>
            </div>

            <button
              className="text-white bg-indigo-500 border-0 py-2 px-6 focus:outline-none hover:bg-indigo-600 rounded text-lg"
              onClick={handleLocationChange}
            >
              {isLoading ? (
                <>
                  <svg
                    aria-hidden="true"
                    role="status"
                    className="inline mr-3 w-4 h-4 text-white animate-spin"
                    viewBox="0 0 100 101"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z"
                      fill="#E5E7EB"
                    ></path>
                    <path
                      d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z"
                      fill="currentColor"
                    ></path>
                  </svg>
                  Loading...
                </>
              ) : (
                "Fetch Layers"
              )}
            </button>
            
            {
              LayersArray.map((item) => {
                return(
                  <>
                    <div className="flex items-center mt-4 w-4/5">
                    <div
                      className="relative inline-block w-10 mr-2 align-middle select-none transition duration-200 ease-in"
                      onClick={() => handleLayerButtonToggle(item)}
                    >
                      {/* <input
                        type="checkbox"
                        name="settlement"
                        id="toggle"
                        value={item.LayerRef.current != null ? item.LayerRef.current.getVisible() : false}
                        className={`toggle-checkbox ${
                          item.LayerRef.current != null ? (item.LayerRef.current.getVisible() ? "right-0 border-green-400" : "left-0") : "left-0"
                        } bg-white absolute block w-6 h-6 rounded-full border-4 appearance-none cursor-pointer`}
                      />
                      <label
                        htmlFor="toggle"
                        className={`toggle-label block overflow-hidden h-6 rounded-full ${
                          item.LayerRef.current != null ? (item.LayerRef.current.getVisible() ? "bg-green-400" : "bg-gray-300") : "bg-gray-300"
                        } cursor-pointer`}
                      ></label> */}
                    </div>
                    <label
                      htmlFor="toggle"
                      className={`text-xs ${
                        isLayersFetched ?"text-gray-700" : "text-gray-400"
                      }`}
                    >
                      {item.name}
                    </label>
                  </div>
                  {item.name !== "Clart Layer" ? <div className="flex">
                    <a
                      className={`flex mx-auto mr-1
                       mt-2 ${isLayersFetched ? 'text-white bg-indigo-500 hover:bg-indigo-600' : 'text-gray-400 bg-gray-300'} border-0 py-2 px-8 focus:outline-none  rounded text-sm cursor-pointer`}
                      onClick={() => handleGeoJsonLayers(item.LayerRef)}
                      href={hrefData}
                      download="Geojsonfeatures.json"
                    >
                      GeoJson
                    </a>
                    <a
                      className={`flex mx-auto mt-2 ${isLayersFetched?'text-white bg-indigo-500 hover:bg-indigo-600' : 'text-gray-400 bg-gray-300' } border-0 py-2 px-8 focus:outline-none  rounded text-sm cursor-pointer`}
                      onClick={() => handleKMLLayers(item.LayerRef)}
                      href={hrefData}
                      download="KMLfeatures.kml"
                    >
                      KML
                    </a>
                  </div> : 
                  <div className="flex">
                    <a
                      className={`flex mx-auto mt-2 ${isLayersFetched?'text-white bg-indigo-500 hover:bg-indigo-600' : 'text-gray-400 bg-gray-300' } border-0 py-2 px-8 focus:outline-none  rounded text-sm cursor-pointer`}
                      onClick={() => handleImageLayers()}
                      href={hrefData}
                      download="KMLfeatures.kml"
                    >
                      GeoTiff
                    </a>
                </div>}
                </>
                )
              })
            }

            <hr className="w-60 h-0.5 bg-gray-400 opacity-30 rounded-full self-center mt-4" />


            <div className="relative mt-3 w-4/5">
              <button
                id="dropdown-button"
                onClick={() => handleMenuToggle(setPlanToggle, planToggle)}
                className={`inline-flex justify-center w-full px-4 py-2 text-sm font-medium ${
                  plans == null ? "text-gray-300" : "text-gray-700"
                } bg-white border border-gray-300 rounded-md shadow-sm focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-100 focus:ring-blue-500`}
              >
                <span className="mr-2">
                  {selectedPlan == null ? "Select Plan" : selectedPlan.plan}
                </span>
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="w-5 h-5 ml-2 -mr-1"
                  viewBox="0 0 20 20"
                  fill="currentColor"
                  aria-hidden="true"
                >
                  <path
                    fillRule="evenodd"
                    d="M6.293 9.293a1 1 0 011.414 0L10 11.586l2.293-2.293a1 1 0 111.414 1.414l-3 3a1 1 0 01-1.414 0l-3-3a1 1 0 010-1.414z"
                    clipRule="evenodd"
                  />
                </svg>
              </button>
              <div
                id="dropdown-menu"
                className={`${
                  block != null && plans != null && planToggle
                    ? "block"
                    : "hidden"
                } z-10 absolute right-0 mt-2 rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5 p-1 space-y-1 h-32 overflow-auto`}
              >
                <input
                  id="search-input"
                  className="block w-full px-4 py-2 text-gray-800 border rounded-md  border-gray-300 focus:outline-none"
                  type="text"
                  placeholder="Search items"
                  autoComplete="off"
                />

                {plans != null &&
                  plans.map((item, idx) => {
                    return (
                      <>
                        <a
                          href="#"
                          key={idx}
                          className="block px-4 py-2 text-gray-700 hover:bg-gray-100 active:bg-blue-100 cursor-pointer rounded-md"
                          onClick={() =>
                            handleItemSelect(
                              setSelectedPlan,
                              item,
                              setPlanToggle
                            )
                          }
                        >
                          {item.plan}
                        </a>
                      </>
                    );
                  })}
              </div>
            </div>

            <button
              className="text-white bg-indigo-500 border-0 py-2 px-6 mt-3 focus:outline-none hover:bg-indigo-600 rounded text-lg"
              onClick={fetchLayers}
            >
              {isLayersLoading ? (
                <>
                  <svg
                    aria-hidden="true"
                    role="status"
                    className="inline mr-3 w-4 h-4 text-white animate-spin"
                    viewBox="0 0 100 101"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z"
                      fill="#E5E7EB"
                    ></path>
                    <path
                      d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z"
                      fill="currentColor"
                    ></path>
                  </svg>
                  Loading...
                </>
              ) : (
                "Fetch Plans"
              )}
            </button>

            {
              PlanLayersArray.map((item) => {
                return(
                  <>
                    <div className="flex items-center mt-4 w-4/5">
                    <div
                      className="relative inline-block w-10 mr-2 align-middle select-none transition duration-200 ease-in"
                      onClick={() => handleLayerButtonToggle(item)}
                    >
                      {/* <input
                        type="checkbox"
                        name="settlement"
                        id="toggle"
                        value={item.LayerRef.current != null ? item.LayerRef.current.getVisible() : false}
                        className={`toggle-checkbox ${
                          item.LayerRef.current != null ? (item.LayerRef.current.getVisible() ? "right-0 border-green-400" : "left-0") : "left-0"
                        } bg-white absolute block w-6 h-6 rounded-full border-4 appearance-none cursor-pointer`}
                      />
                      <label
                        htmlFor="toggle"
                        className={`toggle-label block overflow-hidden h-6 rounded-full ${
                          item.LayerRef.current != null ? (item.LayerRef.current.getVisible() ? "bg-green-400" : "bg-gray-300") : "bg-gray-300"
                        } cursor-pointer`}
                      ></label> */}
                    </div>
                    <label
                      htmlFor="toggle"
                      className={`text-xs ${
                        isOtherLayersFetched ?"text-gray-700" : "text-gray-400"
                      }`}
                    >
                      {item.name}
                    </label>
                  </div>
                  <div className="flex">
                    <a
                      className={`flex mx-auto mr-1
                       mt-2 ${isOtherLayersFetched ? 'text-white bg-indigo-500 hover:bg-indigo-600' : 'text-gray-400 bg-gray-300'} border-0 py-2 px-8 focus:outline-none  rounded text-sm cursor-pointer`}
                      onClick={() => handleGeoJsonLayers(item.LayerRef)}
                      href={hrefData}
                      download="Geojsonfeatures.json"
                    >
                      GeoJson
                    </a>
                    <a
                      className={`flex mx-auto mt-2 ${isOtherLayersFetched?'text-white bg-indigo-500 hover:bg-indigo-600' : 'text-gray-400 bg-gray-300' } border-0 py-2 px-8 focus:outline-none  rounded text-sm cursor-pointer`}
                      onClick={() => handleKMLLayers(item.LayerRef)}
                      href={hrefData}
                      download="KMLfeatures.kml"
                    >
                      KML
                    </a>
                  </div>
                </>
                )
              })
            }

          </div>
        </div>
    </>
  );
};

export default MapArea;
