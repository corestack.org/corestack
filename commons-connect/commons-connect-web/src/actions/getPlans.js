export default async function getPlans(block_id) {
    try {
        let response = await fetch(`https://geoserver.gramvaani.org/api/v1/get_plans/?block_id=${block_id}`, {
            method: "GET",
            headers: {
                "ngrok-skip-browser-warning": "1",
                "Content-Type": "application/json",
                }
            }
        )
    response = await response.json()

    return response.plans

    }catch(e){
        console.log("Not able to Fetch Plans !",e)
    }
}

